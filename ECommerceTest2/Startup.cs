﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ECommerceTest2.Startup))]
namespace ECommerceTest2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
